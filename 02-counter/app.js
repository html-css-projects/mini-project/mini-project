const value = document.getElementById('value');
const buttons = document.querySelectorAll('.btn');
let count = 0;

buttons.forEach(function (button) {
  button.addEventListener('click', function (e) {
    const styles = e.currentTarget.classList[1];

    switch (styles) {
      case 'increase':
        count++;
        break;
      case 'decrease':
        count--;
        break;
      case 'reset':
        count = 0;
        break;
    }

    if (count > 0) {
      value.style.color = 'green';
    }

    if (count < 0) {
      value.style.color = 'red';
    }

    if (count == 0) {
      value.style.color = '#222';
    }

    value.textContent = count;
  });
});
