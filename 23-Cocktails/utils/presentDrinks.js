import displayDrinks from './displayDrinks.js';
import fetchDrinks from './fetchDrinks.js';
import setDrink from './setDrink.js';

const showDrink = async (url) => {
  const data = await fetchDrinks(url);
  const section = await displayDrinks(data);

  if (section) {
    setDrink(section);
  }
};

export default showDrink;
