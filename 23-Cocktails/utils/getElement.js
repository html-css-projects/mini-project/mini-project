const getElement = (selection) => {
  const element = document.querySelector(selection);

  if (element) {
    return element;
  } else {
    throw new Error('No element Selected');
  }
};

export default getElement;
