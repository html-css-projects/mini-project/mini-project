function removeActive(items) {
  items.forEach((removeBtn) => {
    removeBtn.classList.remove('active');
  });
}

export default removeActive;
