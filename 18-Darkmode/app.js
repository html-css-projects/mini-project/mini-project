const toggleBtn = document.querySelector('.btn');
const container = document.querySelector('.articles');

toggleBtn.addEventListener('click', () => {
  if (document.documentElement.classList.contains('dark-theme')) {
    document.documentElement.classList.remove('dark-theme');
  } else {
    document.documentElement.classList.add('dark-theme');
  }
});

const collectionArticle = articles
  .map((article) => {
    const { title, date, length, snippet } = article;

    const formatDate = moment(date).format('MMM Do, YYYY');
    return `<article class="post">
            <h2>
                ${title}
            </h2>

            <div class="post-info">
                <span>
                    ${formatDate}
                </span>

                <span>
                    ${length} min read
                </span>

                <p>
                    ${snippet}
                </p>
            </div>
        </article>`;
  })
  .join('');

container.innerHTML = collectionArticle;
console.log(moment);
